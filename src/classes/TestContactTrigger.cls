@isTest
public with sharing class TestContactTrigger {

    @testSetup static void setup() {
        Profile adminProf = [Select id, Name from Profile where Name = 'System Administrator'];
        Profile testProf = [Select id, Name from Profile where Name = 'CRM Test User'];
        RecordType dealerRT = [Select id from RecordType where DeveloperName = 'Dealer'];

        User adminUser = new User(Alias = 'admin', Country = 'United States', Email = 'email@email.com', EmailEncodingKey = 'UTF-8',
                LastName = 'Admin', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = adminProf.id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'admin@bruno.sandbox.com');
        insert adminUser;
        User testUser = new User(Alias = 'user', Country = 'United States', Email = 'email1@email.com', EmailEncodingKey = 'UTF-8',
                LastName = 'Testing', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = testProf.id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'test@bruno.sandbox.com', ManagerId = adminUser.id);
        insert testUser;

        Account acc = new Account(RecordTypeId = dealerRT.id, Name = 'New Customer');
        insert acc;
    }

    public static testMethod void testcontact() {
        Contact contact = null;
        User testUser = [Select id from User where UserName = 'test@bruno.sandbox.com'];
        User adminUser = [Select id from User where UserName = 'admin@bruno.sandbox.com'];
        // Create - Reject
        System.runAs(testUser) {
            Account acc = [Select id from Account where Cust_Name__c = 'New Customer'];
            contact = new Contact(AccountId = acc.id, LastName = 'Last');
            insert contact;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('No Name');
            request.setAction('Reject');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Create - Approve
        System.runAs(testUser) {
            contact.LastName = 'Name';
            update contact;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Update - Reject
        System.runAs(testUser) {
            contact.Phone = 'Phone';
            update contact;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Phone is non-numeric');
            request.setAction('Reject');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Update - Approve
        System.runAs(testUser) {
            contact.Phone = '5551234567';
            update contact;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Disable - Reject
        System.runAs(testUser) {
            contact.Inactive__c = true;
            update contact;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Not ready to disable');
            request.setAction('Reject');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Disable - Approve
        System.runAs(testUser) {
            contact.Inactive__c = true;
            update contact;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
    }
}