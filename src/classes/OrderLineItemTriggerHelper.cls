/*
 * Created by Jeffrey Zhou on 3/11/16
 */

public class OrderLineItemTriggerHelper {
    
    public static void createLineItemConfigurations(Map<ID, Order_Line_Items__c> oldMap, Map<ID, Order_Line_Items__c> newMap){
        
        List<String> idList = new List<String>(); 
        List<Order_Line_Items__c> oliList = new List<Order_Line_Items__c>(); 
        Boolean isBlankLine = false; 
        
        if(oldMap != null){		//for update triggers
            //Compare the old map with the new map to see if the ConfigurationComment__c field changed
            for(Order_Line_Items__c oldOLI : oldMap.values()){
                for(Order_Line_Items__c newOLI : newMap.values()){
                    if(oldOLI.ConfigurationComment__c != newOLI.ConfigurationComment__c){
                        idList.add(newOLI.Id);//Add the id of the OLI that changed   
                        oliList.add(newOLI);//Add the new OLI that changed
                    }
                }
            }
            //Get all the LIC that has a look to the OLI that has changed and delete them
            List<Line_Item_Configuration__c> LICList = [SELECT ID FROM Line_Item_Configuration__c WHERE Order_Line_Item__c IN :idList];
            if(!LICList.isEmpty()){
                delete LICList;
            }    
        }else{		//for insert triggers
            for(Order_Line_Items__c newOLI : newMap.values()){
                oliList.add(newOLI);
            }
        }
        
        //List of Line_Item_Configuration records to be inserted
        List<Line_Item_Configuration__c> newLICList = new List<Line_Item_Configuration__c>(); 
        
        
        for(Order_Line_Items__c OLI : oliList){
            
          
            //List of strings to be concatenated later
        	List<String> concatenateList = new List<String>(); 
            List<String> splitComments = new List<String>();
            
            //spilt the comment and fill the string list
            if(OLI.ConfigurationComment__c != null){
                splitComments = OLI.ConfigurationComment__c.split('\\r?\\n');     
            }
            Integer c = 0;
            //for each line, create a new configuration record based on the info (description, price, qty, discounted price, if available)
            for(String line : splitComments){		//each line
				//System.debug(line);
                //System.debug(splitComments.get(c));      
                Line_Item_Configuration__c LIC = new Line_Item_Configuration__c();
                LIC.Order_Line_Item__c = oli.Id;
                
                String regex = '\\d*\\,?\\d*\\,?\\d+\\.?\\d*';
                Pattern myPattern = Pattern.compile(regex);
                Matcher myMatcher; 
                
                if(String.isEmpty(line)){
                    isBlankLine = true; 
                }   
                if(isBlankLine == false){
                    if(line.contains('Price:')){//If the line contains the string "Price:"
                        if(line.contains('Discount Price:')){//If the line also contains the string "Disount Price:"
                            String lineAfterDiscountPrice = line.substringAfter('Discount Price:').Trim();//Grab everything after "Discount Price:"
                            if(lineAfterDiscountPrice.contains('Price:')){//If the string grabbed contains the string "Price:", this means that the "Price:" comes after "Discount Price"
                                String discountPrice = '';
                                if(lineAfterDiscountPrice.contains(' ')){
                                    discountPrice = lineAfterDiscountPrice.substringBefore(' ');
                                }
                                //Regular expression
                                myMatcher = myPattern.matcher(discountPrice);
                                if(myMatcher.find()){
                                    discountPrice = myMatcher.group();  
                                    LIC.Discount_Price__c = Decimal.valueOf(discountPrice);
                                }
                                LIC.Description__c = line.substringBefore('Discount Price:');
                                String price = lineAfterDiscountPrice.substringAfter('Price:').Trim();//Grab the string after "Price:" 
                                if(price.contains(' ')){//If the string contains a space
                                    price = price.substringBefore(' ');//Grab the string before the space 
                                }
                                //Regular expression
                                myMatcher = myPattern.matcher(price);
                                if(myMatcher.find()){
                                    price = myMatcher.group();  
                                    LIC.Price__c = Decimal.valueOf(price);//Convert the string to a decimal value to store as the price of the Line Item Configuration record
                                }
                                
                            }
                            if(LIC.Price__c == null){//If the price field is empty
                                String price = line.substringAfter('Price:').trim();//Grab the string after "Price:", with the spaces surrounding trimmed    
                                if(price.contains('Discount Price:')){//If the string grabbed contains "Discount Price:", this means that "Discount Price:" is after "Price:"
                                    if(lineAfterDiscountPrice.contains('')){
                                        lineAfterDiscountPrice = lineAfterDiscountPrice.substringBefore(' ');
                                    }
                                    //Regular Expression
                                    myMatcher = myPattern.matcher(lineAfterDiscountPrice);
                                    if(myMatcher.find()){
                                        lineAfterDiscountPrice = myMatcher.group();  
                                        LIC.Discount_Price__c = Decimal.valueOf(lineAfterDiscountPrice);
                                    }
                                    
                                    LIC.Description__c = line.substringBefore('Price:');
                                    if(price.contains(' ')){//If string contains a space
                                        price = price.substringBefore(' ');//Grab the string before the space
                                    }
                                    //Regular expression
                                    myMatcher = myPattern.matcher(price);
                                    if(myMatcher.find()){
                                        price = myMatcher.group();  
                                        LIC.Price__c = Decimal.valueOf(price);//Convert the string to a decimal value to store as the price of the Line Item Configuration record
                                    }
                                }
                            }
                        }
                        else{//The line does not contain the string "Discount Price:"
                            String price = line.substringAfter('Price:').trim();    
                            if(price.contains(' ')){//If string contains a space
                                price = price.substringBefore(' ');//Grab the string before the space
                            }
                            //Regular expression
                            myMatcher = myPattern.matcher(price);
                            if(myMatcher.find()){
                                price = myMatcher.group();  
                                LIC.Price__c = Decimal.valueOf(price);//Convert the string to a decimal value to store as the price of the Line Item Configuration record
                            }
                            LIC.Description__c = line.substringBefore('Price:');
                        }
                    }
                    else{//No Price property in the line
                        LIC.Description__c = line;    
                    }
                    newLICList.add(LIC);
                }//End of if isBlankLine equals false check
                else{// isBlankLine is true
                    System.debug('HelloWorld');
                    if(String.isNotEmpty(line)){//if the line is not empty
                        concatenateList.add(line); 
                    }
                }
                c++;
            }//End of for loop for split comments
            
            //Only add line item configuration if the concatenateList is not empty
            if(!concatenateList.isEmpty()){
                //Create a new Line Item Configuration record to hold the lines to be concatenated. 
                Line_Item_Configuration__c LIC = new Line_Item_Configuration__c();
                LIC.Order_Line_Item__c = oli.Id;
                LIC.Description__c = '';
                
                //Concatenate all the strings in the list
                for(String line : concatenateList){
                    LIC.Description__c += line + '\n'; 
                }
                System.debug(LIC);
                newLICList.add(LIC); 
            }
        }//End of for loop for OLI
        
        //insert the new line item configuration list
        insert newLICList; 
    } 
    
}