@isTest
public class Test_CommissionTriggerHelper {
    @isTest
    public static void testCommPayoutCreation(){
        List<Profile> profiles = [SELECT Id FROM Profile];

        //create users
        List<User> users = new List<User>();
        User u1 = new User(
                Alias='user1',
                Email='user1@test.com',
                EmailEncodingKey='UTF-8',
                LastName='test1',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                ProfileId = profiles.get(0).Id,
                TimeZoneSidKey='America/Chicago',
                UserName='testuser1@test.com.unittest'
        );
        users.add(u1);
        User u2 = new User(
                Alias='user2',
                Email='user2@test.com',
                EmailEncodingKey='UTF-8',
                LastName='test2',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                ProfileId = profiles.get(0).Id,
                TimeZoneSidKey='America/Chicago',
                UserName='testuser2@test.com.unittest'
        );
        users.add(u2);
        User u3 = new User(
                Alias='user3',
                Email='user3@test.com',
                EmailEncodingKey='UTF-8',
                LastName='test3',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                ProfileId = profiles.get(0).Id,
                TimeZoneSidKey='America/Chicago',
                UserName='testuser3@test.com.unittest'
        );
        users.add(u3);
        insert users;
        //create accounts
        Account a =  new Account(Name = 'Acct 1', OwnerId = u1.Id, FSE__c = u2.Id, ISR__c = u3.Id);
        insert a;

        //create invoices and line items
        List<Invoice__c> invoices = new List<Invoice__c>();
        List<Invoice_Line_Item__c> invLIs = new List<Invoice_Line_Item__c>();
        Invoice__c inv = new Invoice__c(Customer__c = a.Id,
                                        Invoice_Date__c = Date.today().addDays(-10),
                                        Invoice_Amount__c = 900.00);
        insert inv;
        Invoice_Line_Item__c ili = new Invoice_Line_Item__c(Invoice__c = inv.Id,
                                                            Total_Line_Charges__c = 900.00,
                                                            Total_Before_Discount__c = 900.00);
        insert ili;

        //create commission records
        Commission__c comm = new Commission__c(Name = 'Q1 2016 - User1',
                                                Quarter_Start_Date__c = Date.newInstance(2016,1,1),
                                                X90_100_Bonus__c = 3.00,
                                                X100_Bonus__c = 5.00,
                                                Current_Bi_weekly__c = 5000,
                                                Auto_Goal__c = 1000.00,
                                                Access_Goal__c = 1200.00,
                                                Sales_Rep__c = u1.Id);
        insert comm;

        List<Commission_Payout__c> commPayouts = [SELECT Id, Access_Bonus__c, Auto_Bonus__c, Commission__c, Total_Bonus__c FROM Commission_Payout__c];
        System.assertEquals(1, commPayouts.size());

        //should trigger a creation of comm payout record for each commission record

        //check to make sure comm payout records have the correct values
    }
}