@RestResource(urlMapping='/leadcreate/*')
global without sharing class RESTLeadController {

    @HttpPost
    global static String createLeads() {
        String responseBody = '';
        try {
            String requestBody = RestContext.request.requestBody.toString();
            List<Lead> leads = (List<Lead>) JSON.deserialize(requestBody, List<Lead>.class);
            insert leads;
            RestContext.response.statusCode = 200;
            responseBody = leads.size() + ' Leads Created!';
        } catch (Exception e) {
            RestContext.response.statusCode = 500;
            responseBody = e.getMessage() + ' ' + e.getStackTraceString();
        }
        return responseBody;
    }
}