public without sharing class DiffExtension {

    private Map<String, Object> changeSet;
    private Map<String, Object> currentVals;
    private Map<String, String> labelMap;
    private List<String> keySet;
    private boolean noChanges;
    private boolean fieldsNew;

    public DiffExtension(ApexPages.StandardController standardController) {
        labelMap = new Map<String, String>();
        currentVals = new Map<String, Object>();
        keySet = new List<String>();
        SObject sobj = standardController.getRecord();
        List<String> queryFields = new List<String>();

        Map<String, Schema.SObjectField> fieldMap = sobj.getSObjectType().getDescribe().fields.getMap();
        for (Schema.SObjectField fieldObj : fieldMap.values()) {
            Schema.DescribeFieldResult field = fieldObj.getDescribe();
            queryFields.add(field.getName());
            labelMap.put(field.getName(), field.getLabel());
        }
        sobj = Database.query('Select ' + String.join(queryFields, ',') + ' from ' + sobj.getSObjectType() + ' where id = \'' + standardController.getId() + '\'');
        if (sobj.get('Diff__c') != null) {
            changeSet = (Map<String, Object>) JSON.deserializeUntyped((String) sobj.get('Diff__c'));
            if (changeSet.get('DiffIsCreate__c') == true) {
                return;
            }
            changeSet.remove('attributes');
            keySet.addAll(changeSet.keySet());
            keySet.sort();
            for (String key : keySet) {
                if (sobj.get(key) != null) {
                    currentVals.put(key, sobj.get(key));
                } else {
                    currentVals.put(key, '');
                }
                if (changeSet.get(key) == null) {
                    changeSet.put(key, '');
                }
            }
        }
    }

    public Map<String, Object> getChangeSet() {
        return changeSet;
    }

    public List<String> getKeySet() {
        return keySet;
    }

    public Map<String, String> getLabelMap() {
        return labelMap;
    }

    public Map<String, Object> getCurrentVals() {
        return currentVals;
    }

    public boolean getFieldsNew() {
        fieldsNew = changeSet != null && changeSet.containsKey('DiffIsCreate__c') && changeSet.get('DiffIsCreate__c') == true;
        return fieldsNew;
    }

    public boolean getNoChanges() {
        noChanges = (keySet == null || keySet.size() < 1) && (changeSet == null || (changeSet.containsKey('DiffIsCreate__c') && changeSet.get('DiffIsCreate__c') == false));
        return noChanges;
    }
}