/**
 * Created by Ryne on 2/19/2016.
 */

public class RebateTriggerHelper {

    public static void gatherNumbers(List<Rebate__c> rebates){
        Date oldestDate = Date.today();
        Date newestDate = Date.today().addYears(-100);
        Set<Id> accountIds = new Set<Id>();
        Set<Id> vaAccountIds = new Set<Id>();
        Set<String> externalAccountIds = new Set<String>();
        List<RebateWrapper> rebateWrappers = new List<RebateWrapper>();
        List<String> oIds = new List<string>();
        for(Rebate__c rebate : rebates){
            System.debug(rebate.Rebate_Type__c);
            if(rebate.Rebate_Type__c == 'Quarterly VA Install'){

                vaAccountIds.add(rebate.Dealer__c);
                System.debug(vaAccountIds);
                //create a rebateWrapper for this rebate
                /*RebateWrapper rw = new RebateWrapper();
                System.debug(rw);
                rw.rebate = rebate;
                rw.acctId = rebate.Dealer__c;
                System.debug(rw);
                System.debug(rebate.Dealer__r.Ship_Tos__r);
                for(Ship_To__c s : rebate.Dealer__r.Ship_Tos__r){
                    rw.shipTos.add(s.Id);
                    for(Order__c o : s.Orders__r){
                        rw.orderIds.add(o.Id);
                        oIds.add(o.Id);
                    }
                }
                rebateWrappers.add(rw);*/
            }else{
                accountIds.add(rebate.Dealer__c);
            }

            if(rebate.Related_Dealers__c != null){
                List<String> relatedDealers = rebate.Related_Dealers__c.split(';');
                externalAccountIds.addAll(relatedDealers);
            }
            if(rebate.Start_Date__c < oldestDate){
                oldestDate = rebate.Start_Date__c;
            }
            if(rebate.Start_Date__c.addYears(1) > newestDate){
                newestDate = rebate.Start_Date__c.addYears(1);
            }
        }
        Date lastYearOldDate = oldestDate.addYears(-1);
        Date lastYearNewDate = newestDate.addYears(-1);

        //System.debug(rebateWrappers);


        List<Account> accts = [SELECT Id, Customer_ID__c FROM Account WHERE Id in :accountIds OR Customer_ID__c in :externalAccountIds];

        //do work to create rebateWrappers for each VA rebate
        if(!vaAccountIds.isEmpty()){
            List<Ship_To__c> shipToList = [SELECT Id, Customer__c FROM Ship_To__c WHERE Customer__c in :vaAccountIds OR Customer_ID__c in :externalAccountIds];
            List<String> sIds = new List<String>();
            for(Ship_To__c s : shipToList){
                sIds.add(s.Id);
            }
            for(String vaId : vaAccountIds){
                for(Rebate__c r : rebates){
                    RebateWrapper rw = new RebateWrapper();
                    if(r.Dealer__c == vaId){
                        rw.rebate = r;
                        rw.acctId = vaId;
                        for(Ship_To__c s : shipToList){
                            if(s.Customer__c == vaId){
                                rw.shipTos.add(s.Id);
                            }
                        }
                    }
                    rebateWrappers.add(rw);
                }
            }

            System.debug(rebateWrappers);

            List<Order__c> ordersList = [SELECT Id, Ship_To__c FROM Order__c WHERE Ship_To__c in :sIds];
            // make list of ids for orders
            for(Order__c o : ordersList){
                for(RebateWrapper rw : rebateWrappers){
                    for(String sId : rw.shipTos){
                        if(sId == o.Ship_To__c){
                            rw.orderIds.add(o.Id);
                        }
                    }
                }
                oIds.add(o.Id);
            }
            System.debug(rebateWrappers);

            System.debug(oIds);
            if(!oIds.isEmpty()){
                Set<Invoice__c> currentInvoicesForVA = new Set<Invoice__c>([SELECT Id, Invoice_Date__c, Total_Access_Revenue__c, Total_Auto_Revenue__c, Total_Commercial_VPL_Revenue__c, Total_CRE_Revenue__c, Total_Lift_Revenue__c, Total_Residential_VPL_Revenue__c,
                        Total_SRE_Revenue__c, Total_VPL_Revenue__c, Total_VSS_Revenue__c, Customer__c, Pickup_Rebate__c, Order__c FROM Invoice__c
                WHERE Invoice_Date__c >= :oldestDate AND Invoice_Date__c <= :newestDate AND Order__c in :oIds]);
                System.debug(currentInvoicesForVA);

                Set<Invoice__c> lastYearsInvoicesForVA = new Set<Invoice__c>([SELECT Id, Invoice_Date__c, Total_Access_Revenue__c, Total_Auto_Revenue__c, Total_Commercial_VPL_Revenue__c, Total_CRE_Revenue__c, Total_Lift_Revenue__c, Total_Residential_VPL_Revenue__c,
                        Total_SRE_Revenue__c, Total_VPL_Revenue__c, Total_VSS_Revenue__c, Customer__c, Pickup_Rebate__c, Order__c FROM Invoice__c
                WHERE Invoice_Date__c >= :lastYearOldDate AND Invoice_Date__c < :lastYearNewDate AND Order__c in :oIds]);
                System.debug(lastYearsInvoicesForVA);
                for(RebateWrapper rw : rebateWrappers){
                    for(String oId : rw.orderIds){
                        for(Invoice__c ci : currentInvoicesForVA){
                            if(oId == ci.Order__c){
                                rw.currentInvoices.add(ci);
                            }
                        }
                        for(Invoice__c li : lastYearsInvoicesForVA){
                            if(oId == li.Order__c){
                                rw.lyInvoices.add(li);
                            }
                        }
                    }
                }
                System.debug(rebateWrappers);
            }

        }

        Map<String, Id> acctIdsByExtId = new Map<String, Id>();
        for(Account a : accts){
            acctIdsByExtId.put(a.Customer_ID__c, a.Id);
            accountIds.add(a.Id);
        }
        System.debug(acctIdsByExtId);

        //get the invoices for the accounts and date periods
        //make sure all fields are selected
        Set<Invoice__c> currentInvoices = new Set<Invoice__c>([SELECT Id, Invoice_Date__c, Total_Access_Revenue__c, Total_Auto_Revenue__c, Total_Commercial_VPL_Revenue__c, Total_CRE_Revenue__c, Total_Lift_Revenue__c, Total_Residential_VPL_Revenue__c,
                Total_SRE_Revenue__c, Total_VPL_Revenue__c, Total_VSS_Revenue__c, Customer__c, Pickup_Rebate__c FROM Invoice__c
        WHERE Invoice_Date__c >= :oldestDate AND Invoice_Date__c <= :newestDate AND Customer__c in :accountIds]);
        System.debug(currentInvoices);

        Set<Invoice__c> lastYearsInvoices = new Set<Invoice__c>([SELECT Id, Invoice_Date__c, Total_Access_Revenue__c, Total_Auto_Revenue__c, Total_Commercial_VPL_Revenue__c, Total_CRE_Revenue__c, Total_Lift_Revenue__c, Total_Residential_VPL_Revenue__c,
                Total_SRE_Revenue__c, Total_VPL_Revenue__c, Total_VSS_Revenue__c, Customer__c, Pickup_Rebate__c FROM Invoice__c
        WHERE Invoice_Date__c >= :lastYearOldDate AND Invoice_Date__c <= :lastYearNewDate AND Customer__c in :accountIds]);
        System.debug(lastYearsInvoices);



        Map<String, Set<Invoice__c>> currentInvoicesByAccountId = new Map<String, Set<Invoice__c>>();
        Map<String, Set<Invoice__c>> currentVAInvoicesByOrderId = new Map<String, Set<Invoice__c>>();
        Map<String, Set<Invoice__c>> lastYearInvoicesByAccountId = new Map<String, Set<Invoice__c>>();
        Map<String, Set<Invoice__c>> lastYearVAInvoicesByOrderId = new Map<String, Set<Invoice__c>>();
        Set<Invoice__c> tempList1;
        Set<Invoice__c> tempList2;
        Set<Invoice__c> tempList3;
        Set<Invoice__c> tempList4;

        for(Id acctId : accountIds){
            tempList1 = new Set<Invoice__c>();
            for(Invoice__c currentInvoice : currentInvoices){
                if(currentInvoice.Customer__c == acctId){
                    tempList1.add(currentInvoice);
                }
            }
            currentInvoicesByAccountId.put(acctId, tempList1);

            tempList2 = new Set<Invoice__c>();
            for(Invoice__c lastYearInvoice : lastYearsInvoices){
                if(lastYearInvoice.Customer__c == acctId){
                    tempList2.add(lastYearInvoice);
                }
            }
            lastYearInvoicesByAccountId.put(acctId, tempList2);


        }

        /*for(Id oId : oIds){
            tempList3 = new Set<Invoice__c>();
            for(Invoice__c currentVAInvoice : currentInvoicesForVA){
                if(currentVAInvoice.Order__c == oId){
                    tempList3.add(currentVAInvoice);
                }
            }
            currentVAInvoicesByOrderId.put(oId, tempList3);

            tempList4 = new Set<Invoice__c>();
            for(Invoice__c lastYearVAInvoice : lastYearsInvoicesForVA){
                if(lastYearVAInvoice.Order__c == oId){
                    tempList4.add(lastYearVAInvoice);
                }
            }
            lastYearVAInvoicesByOrderId.put(oId, tempList4);
        }*/

        System.debug(currentVAInvoicesByOrderId);
        System.debug(lastYearVAInvoicesByOrderId);


        for(Rebate__c rebate : rebates){
            List<String> relatedDealers = new List<String>();
            if(rebate.Related_Dealers__c != null){
                relatedDealers = rebate.Related_Dealers__c.split(';');
            }
            System.debug(relatedDealers);

            Set<Invoice__c> currentInvoicesForRebate = currentInvoicesByAccountId.get(rebate.Dealer__c);
            Set<Invoice__c> lastYearInvoicesForRebate = lastYearInvoicesByAccountId.get(rebate.Dealer__c);

            //create set of invoices that go with the VA rebates, if any
            Set<Invoice__c> currentVAInvoices = new Set<Invoice__c>();
            Set<Invoice__c> lastYearVAInvoices = new Set<Invoice__c>();
            RebateWrapper rebateWrapper;
            for(RebateWrapper rw : rebateWrappers){
                if(rw.rebate.Id == rebate.Id){
                    rebateWrapper = rw;
                }
            }

            /*if(rebateWrapper != null){
                for(String oId : rebateWrapper.orderIds){
                    currentVAInvoices.addAll(currentVAInvoicesByOrderId.get(oId));
                    lastYearVAInvoices.addAll(lastYearVAInvoicesByOrderId.get(oId));
                }
            }*/


            for(String rd : relatedDealers){
                if(acctIdsByExtId.get(rd) != null){
                    currentInvoicesForRebate.addAll(currentInvoicesByAccountId.get(acctIdsByExtId.get(rd)));
                    lastYearInvoicesForRebate.addAll(lastYearInvoicesByAccountId.get(acctIdsByExtId.get(rd)));
                }

            }

            System.debug(currentInvoicesForRebate);
            System.debug(lastYearInvoicesForRebate);

            //Reset all fields to 0 to start adding
            rebate.Quarterly_Access_Revenue__c = 0;
            rebate.Quarterly_Auto_Revenue__c = 0;
            rebate.Previous_Yr_Qrtly_Access_Revenue__c = 0;
            rebate.Previous_Yr_Qrtly_Auto_Revenue__c = 0;
            rebate.Yearly_Access_Revenue__c = 0;
            rebate.Yearly_Auto_Revenue__c = 0;
            rebate.Previous_Yr_Access_Revenue__c = 0;
            rebate.Previous_Yr_Auto_Revenue__c = 0;
            rebate.Monthly_VPL_Revenue_Non_Buying_Groups__c = 0;
            rebate.Monthly_VPL_Revenue__c = 0;
            rebate.Monthly_SRE_Sales__c = 0;
            rebate.Monthly_CRE_Revenue__c = 0;
            rebate.Monthly_Access_Revenue__c = 0;
            rebate.Monthly_VSS_Revenue__c = 0;
            rebate.Monthly_Lift_Sales__c = 0;
            rebate.Monthly_Auto_Revenue__c = 0;
            rebate.Previous_Yr_Monthly_VPL_Revenue_Non_B__c = 0;
            rebate.Previous_Yr_Monthly_VPL_Revenue__c = 0;
            rebate.Previous_Yr_Monthly_SRE_Revenue__c = 0;
            rebate.Previous_Yr_Monthly_CRE_Revenue__c = 0;
            rebate.Previous_Yr_Monthly_Access_Revenue__c = 0;
            rebate.Previous_Yr_Monthly_VSS_Revenue__c = 0;
            rebate.Previous_Yr_Monthly_Lift_Sales__c = 0;
            rebate.Previous_Yr_Monthly_Auto_Revenue__c = 0;

            //if rebate type is commissions, time period is quarterly or //if rebate type is quarterly growth
            if(rebate.Rebate_Type__c == 'Commissions' || rebate.Rebate_Type__c == 'Quarterly Growth' || (rebate.Rebate_Type__c == 'Buying Group' && rebate.Buying_Group_Time_Frame__c == 'Quarterly')){
                //fill the quarterly revenue fields on the rebate for this year and last year based on start date
                for(Invoice__c i : currentInvoicesForRebate){
                    if(i.Invoice_Date__c >= rebate.Start_Date__c && i.Invoice_Date__c < rebate.Start_Date__c.addMonths(3)){
                        //add the proper fields into the rebate fields
                        if(rebate.Product__c == 'Access' || rebate.Product__c == 'Both'){
                            //grab the access fields
                            //check for pickup
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Quarterly_Access_Revenue__c += i.Total_Access_Revenue__c;
                                System.debug('Rebate Quarterly Access Revenue: ' + rebate.Quarterly_Access_Revenue__c);
                            }

                        }
                        if(rebate.Product__c == 'Auto' || rebate.Product__c == 'Both'){
                            //grab the auto fields
                            System.debug(rebate);
                            System.debug(i);
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Quarterly_Auto_Revenue__c += i.Total_Auto_Revenue__c;
                                System.debug('Rebate Quarterly Auto Revenue: ' + rebate.Quarterly_Auto_Revenue__c);
                            }

                        }
                    }
                }
                //fill quarterly revenue numbers for last years invoices
                for(Invoice__c i : lastYearInvoicesForRebate){
                    if(i.Invoice_Date__c >= rebate.Start_Date__c.addYears(-1) && i.Invoice_Date__c < rebate.Start_Date__c.addYears(-1).addMonths(3)){
                        //add the proper fields into the rebate fields
                        if(rebate.Product__c == 'Access' || rebate.Product__c == 'Both'){
                            //grab the access fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Previous_Yr_Qrtly_Access_Revenue__c += i.Total_Access_Revenue__c;
                            }

                        }
                        if(rebate.Product__c == 'Auto' || rebate.Product__c == 'Both'){
                            //grab the auto fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Previous_Yr_Qrtly_Auto_Revenue__c += i.Total_Auto_Revenue__c;
                            }
                        }
                    }
                }
            }
            //if rebate type is Annual growth, time period is annual
            if(rebate.Rebate_Type__c == 'Annual Growth' || (rebate.Rebate_Type__c == 'Buying Group' && rebate.Buying_Group_Time_Frame__c == 'Yearly')){
                for(Invoice__c i : currentInvoicesForRebate) {
                    if (i.Invoice_Date__c >= rebate.Start_Date__c && i.Invoice_Date__c < rebate.Start_Date__c.addYears(1)) {
                        System.debug(i);
                        if(rebate.Product__c == 'Access' || rebate.Product__c == 'Both'){
                            //grab the access fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                System.debug('Add Access Revenue');
                                rebate.Yearly_Access_Revenue__c += i.Total_Access_Revenue__c;
                            }

                        }
                        if(rebate.Product__c == 'Auto' || rebate.Product__c == 'Both'){
                            //grab the auto fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Yearly_Auto_Revenue__c += i.Total_Auto_Revenue__c;
                            }

                        }
                    }
                }

                for(Invoice__c i : lastYearInvoicesForRebate) {
                    if (i.Invoice_Date__c >= rebate.Start_Date__c.addYears(-1) && i.Invoice_Date__c < rebate.Start_Date__c) {
                        System.debug(i);
                        if(rebate.Product__c == 'Access' || rebate.Product__c == 'Both'){
                            //grab the access fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Previous_Yr_Access_Revenue__c += i.Total_Access_Revenue__c;
                            }

                        }
                        if(rebate.Product__c == 'Auto' || rebate.Product__c == 'Both'){
                            //grab the auto fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Previous_Yr_Auto_Revenue__c += i.Total_Auto_Revenue__c;
                            }

                        }
                    }
                }
            }

            if(rebate.Rebate_Type__c == 'Buying Group' && rebate.Buying_Group_Time_Frame__c == 'Monthly'){
                for(Invoice__c i : currentInvoicesForRebate) {
                    if (i.Invoice_Date__c >= rebate.Start_Date__c && i.Invoice_Date__c < rebate.Start_Date__c.addMonths(1)) {
                        if(rebate.Product__c == 'Access' || rebate.Product__c == 'Both'){
                            //grab the access fields
                            System.debug(rebate.Pickup_Rebate__c + ' : ' + i.Pickup_Rebate__c);
                            System.debug(rebate.Theracare_Non_Buying__c);
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                if(rebate.Theracare_Non_Buying__c){
                                    rebate.Monthly_VPL_Revenue_Non_Buying_Groups__c += i.Total_VPL_Revenue__c;
                                    System.debug(rebate.Monthly_VPL_Revenue_Non_Buying_Groups__c);
                                }else{
                                    rebate.Monthly_VPL_Revenue__c += i.Total_VPL_Revenue__c;
                                }
                                rebate.Monthly_SRE_Sales__c += i.Total_SRE_Revenue__c;
                                rebate.Monthly_CRE_Revenue__c += i.Total_CRE_Revenue__c;
                                rebate.Monthly_Access_Revenue__c += i.Total_Access_Revenue__c;
                            }

                        }
                        if(rebate.Product__c == 'Auto' || rebate.Product__c == 'Both'){
                            //grab the auto fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Monthly_VSS_Revenue__c += i.Total_VSS_Revenue__c;
                                rebate.Monthly_Lift_Sales__c += i.Total_Lift_Revenue__c;
                                rebate.Monthly_Auto_Revenue__c += i.Total_Auto_Revenue__c;
                                System.debug(i.Total_Auto_Revenue__c);
                            }
                        }
                    }
                }

                for(Invoice__c i : lastYearInvoicesForRebate) {
                    if (i.Invoice_Date__c >= rebate.Start_Date__c.addYears(-1) && i.Invoice_Date__c < rebate.Start_Date__c.addYears(-1).addMonths(1)) {
                        if(rebate.Product__c == 'Access' || rebate.Product__c == 'Both'){
                            //grab the access fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                if(rebate.Theracare_Non_Buying__c){
                                    rebate.Previous_Yr_Monthly_VPL_Revenue_Non_B__c += i.Total_VPL_Revenue__c;
                                }else{
                                    rebate.Previous_Yr_Monthly_VPL_Revenue__c += i.Total_VPL_Revenue__c;
                                }
                                rebate.Previous_Yr_Monthly_SRE_Revenue__c += i.Total_SRE_Revenue__c;
                                rebate.Previous_Yr_Monthly_CRE_Revenue__c += i.Total_CRE_Revenue__c;
                                rebate.Previous_Yr_Monthly_Access_Revenue__c += i.Total_Access_Revenue__c;
                            }
                        }
                        if(rebate.Product__c == 'Auto' || rebate.Product__c == 'Both'){
                            //grab the auto fields
                            if((rebate.Pickup_Rebate__c && i.Pickup_Rebate__c) || !rebate.Pickup_Rebate__c){
                                rebate.Previous_Yr_Monthly_VSS_Revenue__c += i.Total_VSS_Revenue__c;
                                rebate.Previous_Yr_Monthly_Lift_Sales__c += i.Total_Lift_Revenue__c;
                                rebate.Previous_Yr_Monthly_Auto_Revenue__c += i.Total_Auto_Revenue__c;
                            }
                        }
                    }
                }
            }
        }
        for(RebateWrapper rw : rebateWrappers){
            rw.rebate.Quarterly_Access_Revenue__c = 0;
            for(Invoice__c ci : rw.currentInvoices){
                if(ci.Invoice_Date__c >= rw.rebate.Start_Date__c && ci.Invoice_Date__c < rw.rebate.Start_Date__c.addMonths(3)){
                    System.debug('current invoice');
                    rw.rebate.Quarterly_Access_Revenue__c += ci.Total_Access_Revenue__c;
                    System.debug(rw.rebate.Quarterly_Access_Revenue__c);
                    System.debug(ci.Total_Access_Revenue__c);
                }
            }

            for(Invoice__c lyi : rw.lyInvoices){
                if(lyi.Invoice_Date__c >= rw.rebate.Start_Date__c.addYears(-1) && lyi.Invoice_Date__c < rw.rebate.Start_Date__c.addYears(-1).addMonths(3)){
                    rw.rebate.Previous_Yr_Qrtly_Access_Revenue__c += lyi.Total_Access_Revenue__c;
                }
            }
        }
    }

    public class RebateWrapper {
        public Rebate__c rebate {get;set;}
        public String acctId {get;set;}
        public List<String> shipTos {get;set;}
        public List<String> orderIds {get;set;}
        public List<Invoice__c> currentInvoices {get;set;}
        public List<Invoice__c> lyInvoices {get;set;}

        public RebateWrapper(){
            //rebate = new Rebate__c();
            shipTos = new List<String>();
            orderIds = new List<String>();
            currentInvoices = new List<Invoice__c>();
            lyInvoices = new List<Invoice__c>();
        }
    }
}