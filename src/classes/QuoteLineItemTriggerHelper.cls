/*
*	Created by Jeffrey Zhou on 3/31/16
*/

public class QuoteLineItemTriggerHelper {
    
    public static void createLineItemConfigurations(Map<ID, Quote_Line_Item__c> oldMap, Map<ID, Quote_Line_Item__c> newMap){
       
        List<id> idList = new List<id>(); 
     
        
        if(oldMap != null){		//for update triggers
            //Compare the old map with the new map to see if the ConfigurationComment__c field changed
            for(Quote_Line_Item__c oldQLI : oldMap.values()){
                for(Quote_Line_Item__c newQLI : newMap.values()){
                    if(oldQLI.Line_Comments__c != newQLI.Line_Comments__c){
                        idList.add(newQLI.Id);//Add the id of the QLI that changed   
                    }
                }
            }
             
        }else{//for insert triggers
            for(Quote_Line_Item__c newQLI : newMap.values()){
                idList.add(newQLI.id);
            }
        }
        
        if(idList.size() > 0){
            futureParseComments(idList);
        }
       
        
        
        
        
    }   
    @future
    public static void futureParseComments(List<Id> qliIds){
        
        //Get all the LIC that has a look to the QLI that has changed and delete them
        List<Line_Item_Configuration__c> LICList = [SELECT ID FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c IN :qliIds];
        if(!LICList.isEmpty()){
            delete LICList;
        }   
        
         //List of Line_Item_Configuration records to be inserted
        List<Line_Item_Configuration__c> newLICList = new List<Line_Item_Configuration__c>(); 
        List<Quote_Line_item__c> qliList = [Select id, line_comments__c from Quote_Line_Item__c where id in :qliIds];
        for(Quote_Line_Item__c QLI : qliList){
            
            
            //List of strings to be concatenated later
            List<String> concatenateList = new List<String>(); 
            List<String> splitComments = new List<String>();
            
            //spilt the comment and fill the string list
            if(QLI.Line_Comments__c != null){
                splitComments = QLI.Line_Comments__c.split('\\r?\\n');     
            }
            Integer c = 0;
            //for each line, create a new configuration record based on the info (description, price, qty, discounted price, if available)
            for(String line : splitComments){		//each line
                //System.debug(line);
                //System.debug(splitComments.get(c));      
                Line_Item_Configuration__c LIC = new Line_Item_Configuration__c();
                LIC.Quote_Line_Item__c = qli.Id;
                
                String regex = '\\d*\\,?\\d*\\,?\\d+\\.?\\d*';
                Pattern myPattern = Pattern.compile(regex);
                Matcher myMatcher; 
                boolean isBlankLine = false;
                if(String.isEmpty(line)){
                    isBlankLine = true; 
                }   
                if(isBlankLine == false){
                    if(line.contains('Price:')){//If the line contains the string "Price:"
                        if(line.contains('Discount Price:')){//If the line also contains the string "Disount Price:"
                            String lineAfterDiscountPrice = line.substringAfter('Discount Price:').Trim();//Grab everything after "Discount Price:"
                            if(lineAfterDiscountPrice.contains('Price:')){//If the string grabbed contains the string "Price:", this means that the "Price:" comes after "Discount Price"
                                String discountPrice = '';
                                if(lineAfterDiscountPrice.contains(' ')){
                                    discountPrice = lineAfterDiscountPrice.substringBefore(' ');
                                }
                                //Regular expression
                                myMatcher = myPattern.matcher(discountPrice);
                                if(myMatcher.find()){
                                    discountPrice = myMatcher.group();  
                                    LIC.Discount_Price__c = Decimal.valueOf(discountPrice);
                                }
                                LIC.Description__c = line.substringBefore('Discount Price:');
                                String price = lineAfterDiscountPrice.substringAfter('Price:').Trim();//Grab the string after "Price:" 
                                if(price.contains(' ')){//If the string contains a space
                                    price = price.substringBefore(' ');//Grab the string before the space 
                                }
                                //Regular expression
                                myMatcher = myPattern.matcher(price);
                                if(myMatcher.find()){
                                    price = myMatcher.group();  
                                    LIC.Price__c = Decimal.valueOf(price);//Convert the string to a decimal value to store as the price of the Line Item Configuration record
                                }
                                
                            }
                            if(LIC.Price__c == null){//If the price field is empty
                                String price = line.substringAfter('Price:').trim();//Grab the string after "Price:", with the spaces surrounding trimmed    
                                if(price.contains('Discount Price:')){//If the string grabbed contains "Discount Price:", this means that "Discount Price:" is after "Price:"
                                    if(lineAfterDiscountPrice.contains('')){
                                        lineAfterDiscountPrice = lineAfterDiscountPrice.substringBefore(' ');
                                    }
                                    //Regular Expression
                                    myMatcher = myPattern.matcher(lineAfterDiscountPrice);
                                    if(myMatcher.find()){
                                        lineAfterDiscountPrice = myMatcher.group();  
                                        LIC.Discount_Price__c = Decimal.valueOf(lineAfterDiscountPrice);
                                    }
                                    
                                    LIC.Description__c = line.substringBefore('Price:');
                                    if(price.contains(' ')){//If string contains a space
                                        price = price.substringBefore(' ');//Grab the string before the space
                                    }
                                    //Regular expression
                                    myMatcher = myPattern.matcher(price);
                                    if(myMatcher.find()){
                                        price = myMatcher.group();  
                                        LIC.Price__c = Decimal.valueOf(price);//Convert the string to a decimal value to store as the price of the Line Item Configuration record
                                    }
                                }
                            }
                        }
                        else{//The line does not contain the string "Discount Price:"
                            String price = line.substringAfter('Price:').trim();    
                            if(price.contains(' ')){//If string contains a space
                                price = price.substringBefore(' ');//Grab the string before the space
                            }
                            //Regular expression
                            myMatcher = myPattern.matcher(price);
                            if(myMatcher.find()){
                                price = myMatcher.group();  
                                LIC.Price__c = Decimal.valueOf(price);//Convert the string to a decimal value to store as the price of the Line Item Configuration record
                            }
                            LIC.Description__c = line.substringBefore('Price:');
                        }
                    }
                    else{//No Price property in the line
                        LIC.Description__c = line;    
                    }
                    newLICList.add(LIC);
                }//End of if isBlankLine equals false check
                else{// isBlankLine is true
                    System.debug('HelloWorld');
                    if(String.isNotEmpty(line)){//if the line is not empty
                        concatenateList.add(line); 
                    }
                }
                c++;
            }//End of for loop for split comments
            
            //Only add line item configuration if the concatenateList is not empty
            if(!concatenateList.isEmpty()){
                //Create a new Line Item Configuration record to hold the lines to be concatenated. 
                Line_Item_Configuration__c LIC = new Line_Item_Configuration__c();
                LIC.Quote_Line_Item__c = qli.Id;
                LIC.Description__c = '';
                
                //Concatenate all the strings in the list
                for(String line : concatenateList){
                    LIC.Description__c += line + '\n'; 
                }
                System.debug(LIC);
                newLICList.add(LIC); 
            }
        }//End of for loop for QLI
        
        //insert the new line item configuration list
        insert newLICList; 
    }
    
}