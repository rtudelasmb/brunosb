global class DeleteBatch implements Database.Batchable<sObject>{

    global final String query {get; set;}

    global DeleteBatch(String query) {
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(this.query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        delete scope;
    }

    global void finish(Database.BatchableContext BC){
    }
}