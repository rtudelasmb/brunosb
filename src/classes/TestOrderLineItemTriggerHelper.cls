/*
* 	Created by Jeffrey Zhou on 3/16/16
*	The SMB Help Desk, inc.
*/

@isTest
public class TestOrderLineItemTriggerHelper {
	
    @testSetup static void setUp(){
        
        Account a = new Account();
        //a.RecordType.Name = 'Dealer'; 
        a.Name = 'Jeffrey'; 
        a.Cust_Name__c = 'Jeffrey';
        insert a; 
        
        Order__c order = new Order__c();
        order.Customer__c = a.id;
        insert order;

        Order_Line_Items__c oli = new Order_Line_Items__c();
        oli.Order__c = order.id; 
        oli.ConfigurationComment__c = 'Model Number: ASL-225^ Price:1550 Discount Price:705.25';
        insert oli; 

        Order_Line_Items__c oli2 = new Order_Line_Items__c();
        oli2.Order__c = order.id; 
        oli2.ConfigurationComment__c = 'Model Number: ASL-225^ Discount Price:705.25 Price:1550';
        insert oli2; 
        
        Order_Line_Items__c oli3 = new Order_Line_Items__c();
        oli3.Order__c = order.id; 
        oli3.ConfigurationComment__c = 'Test Description Discount Price: 200 Blah Price: 300 Blah';
        insert oli3; 
             
    }
    
    static testMethod void createLineItemConfigurationsTest(){
        Order__c order = [SELECT id FROM Order__c];
        Order_Line_Items__c oli = [SELECT id FROM Order_Line_Items__c WHERE Order__c =:order.Id LIMIT 1]; 
        Order_Line_Items__c oli2 = [SELECT id FROM Order_Line_Items__c WHERE Order__c =:order.Id LIMIT 1]; 
        
        /*Test.startTest();
        //Case 1: Line includes Price before Discount Price
        Line_Item_Configuration__c lic = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Order_Line_Item__c =:oli.id LIMIT 1];  
        System.assertEquals(1550, lic.Price__c);
        System.assertEquals(705.25, lic.Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', lic.Description__c);
        
        //Case 2: Line includes Discount Price before Price
        Line_Item_Configuration__c lic2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Order_Line_Item__c =:oli2.id LIMIT 1]; 
        System.assertEquals(1550, lic2.Price__c);
        System.assertEquals(705.25, lic2.Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', lic2.Description__c);
        
        //Update oli to have 2 lines
        oli.ConfigurationComment__c = 'Model Number: ASL-225^ Discount Price:705.25 Price:1550\nVehicle Year: 2015 Price: 1000';
        update oli;
        
        //Case 3: Testing 2 lines
        oli = [SELECT id FROM Order_Line_Items__c WHERE Order__c =:order.Id LIMIT 1]; 
        List<Line_Item_Configuration__c> licList = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Order_Line_Item__c =:oli.id]; 
        System.assertEquals(1550, licList.get(0).Price__c);
        System.assertEquals(705.25, licList.get(0).Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', licList.get(0).Description__c);
        System.debug(licList.get(1));
        System.assertEquals(1000, licList.get(1).Price__c);
        System.assertEquals(null, licList.get(1).Discount_Price__c);
        System.assertEquals('Vehicle Year: 2015', licList.get(1).Description__c);
        
        //Update oli2 to have Discount Price have a word after the value and Price have a word before the value
        oli2.ConfigurationComment__c = 'Test description Discount Price:500Hello Price:World1000';
        update oli2;
        
        //Case 4: Testing the regex written in the helper class
        oli2 = [SELECT id FROM Order_Line_Items__c WHERE Order__c =:order.Id LIMIT 1]; 
        lic2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Order_Line_Item__c =:oli2.id LIMIT 1];     
        System.assertEquals(1000, lic2.Price__c);
        System.assertEquals(500, lic2.Discount_Price__c);
        System.assertEquals('Test description', lic2.Description__c);
        
        //Update oli to have a newline
        oli.ConfigurationComment__c = 'Model Number: ASL-225^ Discount Price:705.25 Price:1550\nVehicle Year: 2015 Price: 1000\nVehicle Make: Acura\n\nHello World\nTotal Price: 2000\nDicount Taken:10';
        update oli;
        
        //Case 5: Testing for a blank space and see if the last Line_Item_Configuration concatenates all lines after the blank space
        oli = [SELECT id FROM Order_Line_Items__c WHERE Order__c =:order.Id LIMIT 1]; 
        licList = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Order_Line_Item__c =:oli.id]; 
        System.assertEquals(1550, licList.get(0).Price__c);
        System.assertEquals(705.25, licList.get(0).Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', licList.get(0).Description__c);
        System.debug(licList.get(1));
        System.assertEquals(1000, licList.get(1).Price__c);
        System.assertEquals(null, licList.get(1).Discount_Price__c);
        System.assertEquals('Vehicle Year: 2015', licList.get(1).Description__c);
        System.assertEquals(null, licList.get(2).Price__c);
        System.assertEquals(null, licList.get(2).Discount_Price__c);
        System.assertEquals('Vehicle Make: Acura', licList.get(2).Description__c);
        System.assertEquals(null, licList.get(3).Price__c);
        System.assertEquals(null, licList.get(3).Discount_Price__c);
        System.assertEquals('Hello World\nTotal Price: 2000\nDicount Taken:10', licList.get(3).Description__c);
               
        //Case 6
        oli2.ConfigurationComment__c = 'Test Description Discount Price: 200 Blah Price: 300 Blah\nTest Description 2 Price: 400 blah';
        update oli2; 
        oli2 = [SELECT id FROM Order_Line_Items__c WHERE Order__c =:order.Id LIMIT 1]; 
        List<Line_Item_Configuration__c> licList2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Order_Line_Item__c =:oli2.id];   
        System.assertEquals(300, licList2.get(0).Price__c);
        System.assertEquals(200, licList2.get(0).Discount_Price__c);
        System.assertEquals('Test Description', licList2.get(0).Description__c);
        System.assertEquals(400, licList2.get(1).Price__c);
        System.assertEquals(null, licList2.get(1).Discount_Price__c);
        System.assertEquals('Test Description 2', licList2.get(1).Description__c);
        
        Test.stopTest();
        */
    } 

    
}