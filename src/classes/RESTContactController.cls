@RestResource(urlMapping='/contacts/*')
global with sharing class RESTContactController {

    @HttpPost
    global static String processContacts() {
        String responseBody = '';
        try {
            String requestBody = RestContext.request.requestBody.toString();
            List<Contact> contacts = (List<Contact>) JSON.deserialize(requestBody, List<Contact>.class);
            // Grab all of the external Ids that came in the accountId field.
            Set<String> accExtId = new Set<String>();
            for (Contact contact : contacts) {
                accExtId.add(contact.AccountId); // These are in reality external ids to the account
            }
            // Select and create a map from ExternalId to the actual Id of the account
            List<Account> accs = [Select id, Epicor_DB_ID__c from Account where Epicor_DB_ID__c in :accExtId];
            Map<String, Id> externalToId = new Map<String, Id>();
            for (Account acc : accs) {
                externalToId.put(acc.Epicor_DB_ID__c, acc.Id);
            }
            // Update the contacts to upsert to point to the actual account id
            for (Contact contact : contacts) {
                contact.AccountId = externalToId.get(contact.AccountId);
            }
            upsert contacts cpextid__c;
            RestContext.response.statusCode = 200;
            responseBody = contacts.size() + ' Contacts Created/Modified!';
        } catch(Exception e) {
            RestContext.response.statusCode = 500;
            responseBody = e.getMessage() + ' ' + e.getStackTraceString();
        }
        return responseBody;
    }
}