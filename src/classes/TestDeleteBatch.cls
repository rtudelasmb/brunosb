@isTest
public class TestDeleteBatch {

    static testMethod void test() {
        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 100; i++) {
            accs.add(new Account(Name = 'Acc: ' + i));
        }
        insert accs;

        Test.startTest();
        Database.executeBatch(new DeleteBatch('Select id from Account'), 100);
        Test.stopTest();

        accs = [Select id from Account];
        System.assertEquals(0, accs.size());
    }
}