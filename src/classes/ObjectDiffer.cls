/**
 * Object differ has the following assumptions:
 * Diff__c is a longTextField on the SObject
 * DiffIsCreate__c is a boolean field on the SObject
 * Trigger that calls this is before insert, after insert and before update.
 */
public class ObjectDiffer {

    public static SObject diff(SObject oldObj, SObject newObj) {
        // if insert, flag it as such so that any update done before approval wont be diffed
        if (oldObj == null) {
            newObj.put('DiffIsCreate__c', true);
            newObj.put('Diff__c', '{"DiffIsCreate__c":true}');
        } else {
            Map<String, Object> tempMap = new Map<String, Object>();
            if (newObj.get('Diff__c') != null) {
                tempMap = (Map<String, Object>) JSON.deserializeUntyped((String) newObj.get('Diff__c'));
            }
            // object was updated after create but before approval
            if (tempMap.get('DiffIsCreate__c') == true) {
                return newObj;
            }
            // Get field Describes for SObject
            Map<String, Schema.SObjectField> fieldMap = newObj.getSObjectType().getDescribe().fields.getMap();
            for (Schema.SObjectField fieldObj : fieldMap.values()) {
                Schema.DescribeFieldResult field = fieldObj.getDescribe();
                if (oldObj.get(field.getName()) != newObj.get(field.getName())) {
                    tempMap.put(field.getName(), oldObj.get(field.getName()));
                }
            }
            newObj.put('Diff__c', JSON.serialize(tempMap));
            if (newObj.get('Diff__c') == '{}') {
                newObj.put('Diff__c', null);
            }
        }
        return newObj;
    }

    // Diffs a list of same-type sobjects
    public static List<SObject> diffAll(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> newList) {
        List<SObject> diffedObjects = new List<SObject>();
        // This is create, use List
        if (newMap == null) {
            for(SObject newObj : newList) {
                diffedObjects.add(diff(null, newObj));
            }
        } else{ // This is an update, use Map
            for(SObject newObj : newMap.values()) {
                diffedObjects.add(diff(oldMap.get(newObj.Id), newObj));
            }
        }
        return diffedObjects;
    }

    // Reverts values of newObj to values from the Diff__c field
    public static SObject revert(Sobject newObj) {
        Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped((String)newObj.get('Diff__c'));
        for (String key : tempMap.keySet()) {
            newObj.put(key, tempMap.get(key));
        }
        return newObj;
    }
}