@isTest
public with sharing class TestAccountTrigger {

    @testSetup static void setup() {
        Profile adminProf = [Select id, Name from Profile where Name = 'System Administrator'];
        Profile testProf = [Select id, Name from Profile where Name = 'CRM Test User'];

        User adminUser = new User(Alias = 'admin', Country = 'United States', Email = 'email@email.com', EmailEncodingKey = 'UTF-8',
                LastName = 'Admin', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = adminProf.id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'admin@bruno.sandbox.com');
        insert adminUser;
        User testUser = new User(Alias = 'user', Country = 'United States', Email = 'email1@email.com', EmailEncodingKey = 'UTF-8',
                LastName = 'Testing', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = testProf.id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'test@bruno.sandbox.com', ManagerId = adminUser.id);
        insert testUser;
    }

    public static testMethod void testaccount() {
        Account account = null;
        User testUser = [Select id from User where UserName = 'test@bruno.sandbox.com'];
        User adminUser = [Select id from User where UserName = 'admin@bruno.sandbox.com'];
        RecordType dealerRT = [Select id from RecordType where DeveloperName = 'Dealer'];
        // Create - Reject
        System.runAs(testUser) {
            account = new Account(RecordTypeId = dealerRT.Id, Name = 'TestName');
            insert account;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('No Site');
            request.setAction('Reject');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Create - Approve
        System.runAs(testUser) {
            account.Website = 'TestSite';
            update account;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Update - Reject
        System.runAs(testUser) {
            account.Phone = 'Phone';
            update account;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Phone is non-numeric');
            request.setAction('Reject');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Update - Approve
        System.runAs(testUser) {
            account.Phone = '5551234567';
            update account;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Disable - Reject
        System.runAs(testUser) {
            account.Inactive__c = true;
            update account;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Not ready to disable');
            request.setAction('Reject');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Disable - Approve
        System.runAs(testUser) {
            account.Inactive__c = true;
            update account;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
    }
}