@isTest
public class TestDiffExtension {

    static testMethod void testDiffExtension() {
        Profile adminProf = [Select id, Name from Profile where Name = 'System Administrator'];
        Profile testProf = [Select id, Name from Profile where Name = 'CRM Test User'];
        RecordType dealerRT = [Select id from RecordType where DeveloperName = 'Dealer'];

        User adminUser = new User(Alias = 'admin', Country = 'United States', Email = 'email@email.com', EmailEncodingKey = 'UTF-8',
                LastName = 'Admin', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = adminProf.id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'admin@bruno.sandbox.com');
        insert adminUser;
        User testUser = new User(Alias = 'user', Country = 'United States', Email = 'email1@email.com', EmailEncodingKey = 'UTF-8',
                LastName = 'Testing', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = testProf.id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'test@bruno.sandbox.com', ManagerId = adminUser.id);
        insert testUser;

        Account acc = new Account(RecordTypeId = dealerRT.id, Name = 'New Customer');
        insert acc;

        // Using shipTo, but you can use any object to test this.
        Ship_To__c shipTo = null;
        System.runAs(testUser) {
            shipTo = new Ship_To__c(Customer__c = acc.id);
            insert shipTo;
        }
        System.runAs(adminUser) {
            ProcessInstance instance = [Select id from ProcessInstance where targetObjectId = :shipTo.id and status = 'Pending'];
            ProcessInstanceWorkItem workItem = [Select id, originalActorId from ProcessInstanceWorkItem where ProcessInstanceId = :instance.id limit 1];
            Approval.ProcessWorkItemRequest request = new Approval.ProcessWorkItemRequest();
            request.setWorkItemId(workItem.id);
            request.setComments('Approved!');
            request.setAction('Approve');
            Approval.ProcessResult result = Approval.process(request);
        }
        // Update to get all the diffs after create
        System.runAs(testUser) {
            shipTo.Name__c = 'Name';
            shipTo.Phone__c = 'Phone';
            shipTo.Address_1__c = 'Address1';
            shipTo.Address_2__c = 'Address2';
            shipTo.Address_3__c = 'Address3';
            update shipTo;
        }

        Test.setCurrentPage(Page.ShipTo_Diff);
        DiffExtension extension = new DiffExtension(new ApexPages.StandardController(shipTo));
        extension.getChangeSet();
        extension.getKeySet();
        extension.getLabelMap();
        extension.getCurrentVals();
        extension.getFieldsNew();
        extension.getNoChanges();
    }
}