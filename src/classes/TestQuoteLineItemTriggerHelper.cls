/*
*	Created by Jeffrey Zhou on 3/31/16
*	The SMB Help Desk, inc.
*/

@isTest
public class TestQuoteLineItemTriggerHelper {
		
        @testSetup public static void setUp(){
        
        Account a = new Account();
        //a.RecordType.Name = 'Dealer'; 
        a.Name = 'Jeffrey'; 
        a.Cust_Name__c = 'Jeffrey';
        insert a; 
        
        Quote__c quote = new Quote__c();
        quote.Customer__c = a.id;
        insert quote;

        Quote_Line_Item__c qli = new Quote_Line_Item__c();
        qli.quote__c = quote.id; 
        qli.Line_Comments__c = 'Model Number: ASL-225^ Price:1550 Discount Price:705.25';
        insert qli; 

        Quote_Line_Item__c qli2 = new Quote_Line_Item__c();
        qli2.quote__c = quote.id; 
        qli2.Line_Comments__c = 'Model Number: ASL-225^ Discount Price:705.25 Price:1550';
        insert qli2; 
        
        Quote_Line_Item__c qli3 = new Quote_Line_Item__c();
        qli3.quote__c = quote.id; 
        qli3.Line_Comments__c = 'Test Description Discount Price: 200 Blah Price: 300 Blah';
        insert qli3; 

           
    }
    
    static testMethod void createLineItemConfigurationsTest(){

        Quote__c quote = [SELECT id FROM quote__c];
        List<Quote_Line_Item__c> qlis = [SELECT id FROM Quote_Line_Item__c WHERE quote__c =:quote.Id ]; 
        Quote_Line_Item__c qli = qlis.get(0);
        Quote_Line_Item__c qli2 = qlis.get(1); 
        /*
        //Case 1: Line includes Price before Discount Price
        Line_Item_Configuration__c lic = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli.id LIMIT 1];  
        System.assertEquals(1550, lic.Price__c);
        System.assertEquals(705.25, lic.Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', lic.Description__c);
        
        //Case 2: Line includes Discount Price before Price
        Line_Item_Configuration__c lic2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli2.id LIMIT 1]; 
        System.assertEquals(1550, lic2.Price__c);
        System.assertEquals(705.25, lic2.Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', lic2.Description__c);
     	*/
    } 
    
    /*
    static testMethod void updatelitest(){
       
        
        Test.startTest();
        setup();
         
        Quote__c quote = [SELECT id FROM quote__c];
         List<Quote_Line_Item__c> qlis = [SELECT id FROM Quote_Line_Item__c WHERE quote__c =:quote.Id ]; 
        Quote_Line_Item__c qli = qlis.get(0);
        Quote_Line_Item__c qli2 = qlis.get(1); 
         //Update qli to have 2 lines
        qli.Line_Comments__c = 'Model Number: ASL-225^ Discount Price:705.25 Price:1550\nVehicle Year: 2015 Price: 1000';
        update qli;
        
        //Update qli2 to have Discount Price have a word after the value and Price have a word before the value
        qli2.Line_Comments__c = 'Test description Discount Price:500Hello Price:World1000';
        update qli2;
        
        Test.stopTest();
                Line_Item_Configuration__c lic = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli.id LIMIT 1];  
        Line_Item_Configuration__c lic2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli2.id LIMIT 1]; 

        //Case 3: Testing 2 lines
        qli = [SELECT id FROM Quote_Line_Item__c WHERE Quote__c =:quote.Id LIMIT 1]; 
        List<Line_Item_Configuration__c> licList = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli.id]; 
        System.assertEquals(1550, licList.get(0).Price__c);
        System.assertEquals(705.25, licList.get(0).Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', licList.get(0).Description__c);
        System.debug(licList.get(1));
        System.assertEquals(1000, licList.get(1).Price__c);
        System.assertEquals(null, licList.get(1).Discount_Price__c);
        System.assertEquals('Vehicle Year: 2015', licList.get(1).Description__c);
        
        
        //Case 4: Testing the regex written in the helper class
        lic2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli2.id LIMIT 1];     
        System.assertEquals(1000, lic2.Price__c);
        System.assertEquals(500, lic2.Discount_Price__c);
        System.assertEquals('Test description', lic2.Description__c);
        
        
    }
    
    static testMethod void test3(){
        
        
        Test.startTest();
        
        setup();
        Quote__c quote = [SELECT id FROM quote__c];
         List<Quote_Line_Item__c> qlis = [SELECT id FROM Quote_Line_Item__c WHERE quote__c =:quote.Id ]; 
        Quote_Line_Item__c qli = qlis.get(0);
        Quote_Line_Item__c qli2 = qlis.get(1); 
        //Update qli to have a newline
        qli.Line_Comments__c = 'Model Number: ASL-225^ Discount Price:705.25 Price:1550\nVehicle Year: 2015 Price: 1000\nVehicle Make: Acura\n\nHello World\nTotal Price: 2000\nDicount Taken:10';
        
         //Case 6
        qli2.Line_Comments__c = 'Test Description Discount Price: 200 Blah Price: 300 Blah\nTest Description 2 Price: 400 blah';
        update new List<Quote_Line_Item__c> {qli, qli2}; 
        
        Test.stopTest();
        
        //Case 5: Testing for a blank space and see if the last Line_Item_Configuration concatenates all lines after the blank space
        List<Line_Item_Configuration__c> licList = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli.id]; 
        System.assertEquals(1550, licList.get(0).Price__c);
        System.assertEquals(705.25, licList.get(0).Discount_Price__c);
        System.assertEquals('Model Number: ASL-225^', licList.get(0).Description__c);
        System.debug(licList.get(1));
        System.assertEquals(1000, licList.get(1).Price__c);
        System.assertEquals(null, licList.get(1).Discount_Price__c);
        System.assertEquals('Vehicle Year: 2015', licList.get(1).Description__c);
        System.assertEquals(null, licList.get(2).Price__c);
        System.assertEquals(null, licList.get(2).Discount_Price__c);
        System.assertEquals('Vehicle Make: Acura', licList.get(2).Description__c);
        System.assertEquals(null, licList.get(3).Price__c);
        System.assertEquals(null, licList.get(3).Discount_Price__c);
               
       
        List<Line_Item_Configuration__c> licList2 = [SELECT id, Price__c, Discount_Price__c, Description__c FROM Line_Item_Configuration__c WHERE Quote_Line_Item__c =:qli2.id];   
        System.assertEquals(300, licList2.get(0).Price__c);
        System.assertEquals(200, licList2.get(0).Discount_Price__c);
        System.assertEquals('Test Description', licList2.get(0).Description__c);
        System.assertEquals(400, licList2.get(1).Price__c);
        System.assertEquals(null, licList2.get(1).Discount_Price__c);
        System.assertEquals('Test Description 2', licList2.get(1).Description__c);
    }
	*/
    
}