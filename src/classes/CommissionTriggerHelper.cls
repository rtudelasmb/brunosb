public class CommissionTriggerHelper {
    public static void createCommissionPayouts(List<Commission__c> commissions){
        //get accounts that the users belong to
        //get invoices for these accounts that fit in the quarter
        //get the date range
       /* Date oldestDate = Date.today();
        Date newestDate = Date.today().addYears(-100);
        List<Id> userIds = new List<Id>();
        for(Commission__c c : commissions){
            userIds.add(c.Sales_Rep__c);
            if(c.Quarter_Start_Date__c < oldestDate){
                oldestDate = c.Quarter_Start_Date__c;
            }
            if(c.Quarter_Start_Date__c.addMonths(3) > newestDate){
                newestDate = c.Quarter_Start_Date__c.addMonths(3);
            }
        }


        List<Account> accounts = [SELECT Id, Name, FSE__c, ISR__c, OwnerId,
                                        (SELECT Id, Invoice_Date__c, Auto_Revenue__c FROM Invoices__r WHERE Invoice_Date__c >= :oldestDate AND Invoice_Date__c <= :newestDate)
                                    FROM Account WHERE OwnerId in :userIds OR FSE__c in: userIds OR ISR__c in :userIds];
        List<Commission_Payout__c> commPayouts = new List<Commission_Payout__c>();
        //for each account, get the invoice totals for access revenues and auto revenues
        for(Account a : accounts){
            for(Commission__c c : commissions){
                if(c.Sales_Rep__c == a.OwnerId || c.Sales_Rep__c == a.FSE__c ||c.Sales_Rep__c == a.ISR__c){ //if the user for the commission record is part of this account, then check the numbers
                    Commission_Payout__c cp = new Commission_Payout__c(Name = c.Name + ' Payout', Commission__c = c.Id, OwnerId = c.Sales_Rep__c);
                    Decimal totalAccessRevenue = 0;
                    Decimal totalAutoRevenue = 0;
                    for(Invoice__c inv : a.Invoices__r){
                        if(inv.Invoice_Date__c >= c.Quarter_Start_Date__c && inv.Invoice_Date__c <= c.Quarter_Start_Date__c.addMonths(3)){
                            //totalAccessRevenue += inv.Access_Revenue__c;
                            totalAutoRevenue += inv.Auto_Revenue__c;
                        }
                    }
                    System.debug('Total Access Revenue: ' + totalAccessRevenue);
                    cp.Total_Access_Revenue__c = totalAccessRevenue;
                    cp.Total_Auto_Revenue__c = totalAutoRevenue;
                    System.debug('Total Auto Revenue: ' + totalAutoRevenue);
                    //access check
                    if(totalAccessRevenue >= (c.Access_Goal__c * .9)){    //90% to 100%
                        cp.Access_Bonus__c = (((totalAccessRevenue / c.Access_Goal__c) - 0.9) * 100) * ((c.X90_100_Bonus__c / 100) * (c.Quarterly_Wage__c / 2));
                    }
                    if(totalAccessRevenue > c.Access_Goal__c){       //over 100%
                        cp.Access_Bonus__c += (((totalAccessRevenue / c.Access_Goal__c) - 1) * 100) * (((c.X100_Bonus__c / 100) - (c.X90_100_Bonus__c / 100)) * (c.Quarterly_Wage__c / 2));
                    }
                    //auto check
                    if(totalAutoRevenue >= (c.Auto_Goal__c * .9)){    //90% to 100%
                        cp.Auto_Bonus__c = (((totalAutoRevenue / c.Auto_Goal__c) - 0.9) * 100) * ((c.X90_100_Bonus__c / 100) * (c.Quarterly_Wage__c / 2));
                    }
                    if(totalAutoRevenue > c.Auto_Goal__c){       //over 100%
                        cp.Auto_Bonus__c += (((totalAutoRevenue / c.Auto_Goal__c) - 1) * 100) * (((c.X100_Bonus__c / 100) - (c.X90_100_Bonus__c / 100)) * (c.Quarterly_Wage__c / 2));
                    }
                    commPayouts.add(cp);
                }
            }
        }
        System.debug(commPayouts);
        insert commPayouts;*/

    }
}