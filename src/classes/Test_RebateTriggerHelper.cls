/**
 * Created by Ryne on 3/18/2016.
 */
@isTest
public with sharing class Test_RebateTriggerHelper {

    /***********************************************
    This is the setup for the rebate test. Create all prerequired records and insert
        Setup 2 accounts of record type Dealer with Name, Cust Name, and Customer ID filled
        Setup Ship VIA record called Pickup
        Setup 2 orders for each account, one each linked to the pickup ship via
        Setup invoices for each account, link some to each order so some are pickup and some arent
        Setup AR journal entries for each invoice - fill Credit Amount field
     ***********************************************/
    @testSetup
    public static void setup(){
        //create 2 accounts
        RecordType dealer = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Dealer'];
        Account acct1 = new Account(Name = 'IL-1001/Star Labs', Cust_Name__c = 'Star Labs', Customer_ID__c = 'IL-1001');
        Account acct2 = new Account(Name = 'IL-1002/Mercury Labs', Cust_Name__c = 'Mercury Labs', Customer_ID__c = 'IL-1002');
        Account acct3 = new Account(Name = 'IL-1003/Moon Labs', Cust_Name__c = 'Moon Labs', Customer_ID__c = 'IL-1003');
        insert new List<Account> {acct1, acct2, acct3};

        //setup ship to for va rebate test
        Ship_To__c shipTo = new Ship_To__c(Customer__c = acct3.Id);
        insert shipTo;

        //setup ship via record called pick up
        Ship_Via__c pickup = new Ship_Via__c(Name = 'PICK UP');
        insert pickup;

        //setup 2 orders for each account, one each linked to the pickup
        Order__c order1 = new Order__c(Customer__c = acct1.Id);
        Order__c order2 = new Order__c(Customer__c = acct1.Id, Ship_Via__c = pickup.Id);
        Order__c order3 = new Order__c(Customer__c = acct2.Id);
        Order__c order4 = new Order__c(Customer__c = acct2.Id, Ship_Via__c = pickup.Id);
        //set up 2 orders for the va test
        Order__c order5 = new Order__c(Customer__c = acct3.Id, Ship_To__c = shipTo.Id);
        Order__c order6 = new Order__c(Customer__c = acct3.Id, Ship_To__c = shipTo.Id);
        insert new List<Order__c> {order1, order2, order3, order4, order5, order6};

        //setup invoices for each account and link some to each order so some are pickup
        List<Invoice__c> invoices = new List<Invoice__c>();
        Invoice__c i1 = new Invoice__c(Customer__c = acct1.Id, Order__c = order1.Id, Invoice_Date__c = Date.today());
        Invoice__c i2 = new Invoice__c(Customer__c = acct1.Id, Order__c = order2.Id, Invoice_Date__c = Date.today());

        Invoice__c i3 = new Invoice__c(Customer__c = acct2.Id, Order__c = order3.Id, Invoice_Date__c = Date.today());
        Invoice__c i4 = new Invoice__c(Customer__c = acct2.Id, Order__c = order4.Id, Invoice_Date__c = Date.today());

        Invoice__c i5 = new Invoice__c(Customer__c = acct1.Id, Order__c = order1.Id, Invoice_Date__c = Date.today().addYears(-1));
        Invoice__c i6 = new Invoice__c(Customer__c = acct1.Id, Order__c = order2.Id, Invoice_Date__c = Date.today().addYears(-1));

        Invoice__c i7 = new Invoice__c(Customer__c = acct2.Id, Order__c = order3.Id, Invoice_Date__c = Date.today().addYears(-1));
        Invoice__c i8 = new Invoice__c(Customer__c = acct2.Id, Order__c = order4.Id, Invoice_Date__c = Date.today().addYears(-1));

        Invoice__c i9 = new Invoice__c(Customer__c = acct3.Id, Order__c = order5.Id, Invoice_Date__c = Date.today());
        Invoice__c i10 = new Invoice__c(Customer__c = acct3.Id, Order__c = order5.Id, Invoice_Date__c = Date.today().addYears(-1));

        invoices.add(i1);
        invoices.add(i2);
        invoices.add(i3);
        invoices.add(i4);
        invoices.add(i5);
        invoices.add(i6);
        invoices.add(i7);
        invoices.add(i8);
        invoices.add(i9);
        invoices.add(i10);
        insert invoices;

        //setup AR Journal entries for each order
        List<AR_Journal_Entry_Line__c> arLines = new List<AR_Journal_Entry_Line__c>();
        List<String> glAccounts = new List<String>{'4100|35|00', '4100|35|00', '4100|10|00', '4100|10|00', '4100|10|00', '4100|10|00', '4100|35|00', '4100|35|00', '4100|35|00', '4100|35|00'};
        for(Integer i = 0; i < 10; i++){
            AR_Journal_Entry_Line__c ar = new AR_Journal_Entry_Line__c(Invoice__c = invoices.get(i).Id, Name = 'Test Entry ' + i, Credit_Amount__c = 1000, GL_Account__c = glAccounts.get(i) );
            arLines.add(ar);
        }
        insert arLines;
    }


    /******************************************************
        Create rebates for each time period type, as well as a rebate that has a value in the related dealer field
     *****************************************************/
    @isTest
    public static void createCommissionRebate(){
        List<Account> accounts = [SELECT Id FROM Account];
        Rebate__c rebate1 = new Rebate__c(Name = 'Test Rebate 1', Dealer__c = accounts.get(0).Id, Start_Date__c = Date.today(), Product__c = 'Both', Rebate_Type__c = 'Commissions');
        insert rebate1;

        rebate1 = [SELECT Id, Quarterly_Access_Revenue__c, Yearly_Access_Revenue__c FROM Rebate__c LIMIT 1];
        System.assertEquals(2000, rebate1.Quarterly_Access_Revenue__c);
        System.assertEquals(0, rebate1.Yearly_Access_Revenue__c);
    }

    @isTest
    public static void createAnnualGrowthRebate(){
        List<Account> accounts = [SELECT Id FROM Account];
        Rebate__c rebate1 = new Rebate__c(Name = 'Test Rebate 1', Dealer__c = accounts.get(0).Id, Start_Date__c = Date.today(), Product__c = 'Both', Rebate_Type__c = 'Annual Growth');
        insert rebate1;

        rebate1 = [SELECT Id, Yearly_Access_Revenue__c, Quarterly_Access_Revenue__c FROM Rebate__c LIMIT 1];
        System.assertEquals(2000, rebate1.Yearly_Access_Revenue__c);
        System.assertEquals(0, rebate1.Quarterly_Access_Revenue__c);
    }
    @isTest
    public static void createQuarterlyGrowthRebate(){
        List<Account> accounts = [SELECT Id FROM Account];
        Rebate__c rebate1 = new Rebate__c(Name = 'Test Rebate 1', Dealer__c = accounts.get(0).Id, Start_Date__c = Date.today(), Product__c = 'Both', Rebate_Type__c = 'Quarterly Growth');
        insert rebate1;

        rebate1 = [SELECT Id, Yearly_Access_Revenue__c, Quarterly_Access_Revenue__c FROM Rebate__c LIMIT 1];
        System.assertEquals(0, rebate1.Yearly_Access_Revenue__c);
        System.assertEquals(2000, rebate1.Quarterly_Access_Revenue__c);
    }
    @isTest
    public static void createBuyingGroupMonthlyRebate(){
        List<Account> accounts = [SELECT Id FROM Account];
        Rebate__c rebate1 = new Rebate__c(Name = 'Test Rebate 1', Dealer__c = accounts.get(0).Id, Start_Date__c = Date.today(), Product__c = 'Both', Rebate_Type__c = 'Buying Group', Buying_Group_Time_Frame__c = 'Monthly');
        insert rebate1;

        rebate1 = [SELECT Id, Yearly_Access_Revenue__c, Monthly_Access_Revenue__c FROM Rebate__c LIMIT 1];
        System.assertEquals(0, rebate1.Yearly_Access_Revenue__c);
        System.assertEquals(2000, rebate1.Monthly_Access_Revenue__c);
    }

    @isTest
    public static void createBuyingGroupMonthlyRebateWithNonBuying(){
        List<Account> accounts = [SELECT Id FROM Account];
        Rebate__c rebate1 = new Rebate__c(Name = 'Test Rebate 1', Dealer__c = accounts.get(0).Id, Start_Date__c = Date.today(), Product__c = 'Both', Rebate_Type__c = 'Buying Group', Buying_Group_Time_Frame__c = 'Monthly', Theracare_Non_Buying__c = true);
        insert rebate1;

        rebate1 = [SELECT Id, Yearly_Access_Revenue__c, Monthly_Access_Revenue__c, Monthly_VPL_Revenue_Non_Buying_Groups__c FROM Rebate__c LIMIT 1];
        System.assertEquals(0, rebate1.Yearly_Access_Revenue__c);
        System.assertEquals(2000, rebate1.Monthly_Access_Revenue__c);
        System.assertEquals(2000, rebate1.Monthly_VPL_Revenue_Non_Buying_Groups__c);
    }
    @isTest
    public static void createRebateWithRelatedDealers(){
        List<Account> accounts = [SELECT Id FROM Account];
        Rebate__c rebate1 = new Rebate__c(Name = 'Test Rebate 1', Dealer__c = accounts.get(0).Id, Start_Date__c = Date.today(), Product__c = 'Both',
                Rebate_Type__c = 'Buying Group', Buying_Group_Time_Frame__c = 'Monthly', Theracare_Non_Buying__c = true, Related_Dealers__c = 'IL-1001;IL-1002');
        insert rebate1;

        rebate1 = [SELECT Id, Yearly_Access_Revenue__c, Monthly_Auto_Revenue__c, Monthly_VPL_Revenue_Non_Buying_Groups__c FROM Rebate__c LIMIT 1];
        System.assertEquals(0, rebate1.Yearly_Access_Revenue__c);
        System.assertEquals(2000, rebate1.Monthly_Auto_Revenue__c);
        System.assertEquals(2000, rebate1.Monthly_VPL_Revenue_Non_Buying_Groups__c);
    }

    @isTest
    public static void createRebateWithVAType(){
        List<Account> accts = [SELECT Id FROM Account WHERE Name = 'IL-1003/Moon Labs'];
        System.debug(accts);
        Rebate__c rebate = new Rebate__c(Name = 'Test Rebate', Dealer__c = accts.get(0).Id, Start_Date__c = Date.today(), Rebate_Type__c = 'Quarterly VA Install');
        insert rebate;
    }
}