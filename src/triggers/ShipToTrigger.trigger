trigger ShipToTrigger on Ship_To__c (before insert, after insert, before update, after update) {

    System.debug('It got here');

    Profile profile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
    // Only run diff if the user is not a system administrator
    if(profile.Name != 'System Administrator' && Trigger.isBefore) {
        List<SObject> sobjs = ObjectDiffer.diffAll(Trigger.oldMap, Trigger.newMap, Trigger.new);
        if (Trigger.isUpdate) {
            for (SObject sObj : sobjs) {
                if (sObj.get('Inactive__c') == false) { // On every update, if not explicitly disabled, disable it
                    sObj.put('Inactive__c', true);
                }
                sObj.put('IsApproved__c', false);
                sObj.put('Is_Rejected__c', false);
                if (sObj.get('IsRecalled__c') == true) { // When recalled, revert object and clear flag.
                    ObjectDiffer.revert(sObj);
                    sObj.put('IsRecalled__c', false);
                    sObj.put('Diff__c', null);
                }
            }
        }
    }
    if(profile.Name == 'System Administrator' && Trigger.isBefore) {
        for (Ship_To__c shipTo : Trigger.new) {
            if (shipTo.Is_Rejected__c == true) {
                if (shipTo.DiffIsCreate__c != true) {
                    Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped(shipTo.Diff__c);
                    if (!tempMap.containsKey('Inactive__c') && !(shipTo.Name__c == 'DO NOT USE' || (tempMap.containsKey('Name__c') && tempMap.get('Name__c') == 'DO NOT USE'))) {
                        shipTo.Inactive__c = !shipTo.Inactive__c;
                    }
                    ObjectDiffer.revert(shipTo);
                    shipTo.Diff__c = null;
                }
            }
            if (shipTo.IsApproved__c == true) {
                if (shipTo.DiffIsCreate__c != true) {
                    if (shipTo.Diff__c != null) {
                        Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped(shipTo.Diff__c);
                        if (tempMap.containsKey('Inactive__c') && tempMap.get('Inactive__c') != true) {
                            shipTo.Inactive__c = true;
                            shipTo.Name__c = 'DO NOT USE';
                            shipTo.Address_1__c = null;
                        } else {
                            shipTo.Inactive__c = false;
                        }
                    } else {
                        shipTo.Inactive__c = false;
                    }
                } else {
                    shipTo.Inactive__c = false;
                }
                shipTo.Diff__c = null;
            }
        }
    }
    if (profile.Name != 'System Administrator' && Trigger.isAfter) {
        for (Ship_To__c shipTo : Trigger.new) {
            List<ProcessInstance> instances = [Select id from ProcessInstance where targetObjectId = :shipTo.id and status = 'Pending'];
            if (instances.size() > 0) {return;} // if item is already in approval due to workflow updates.
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            User user = [Select id, ManagerId from User where id = :UserInfo.getUserId()];
            approvalRequest.setSubmitterId(UserInfo.getUserId());
            approvalRequest.setNextApproverIds(new List<Id>{user.ManagerId});
            approvalRequest.setObjectId(shipTo.Id);
            approvalRequest.setProcessDefinitionNameOrId('Changes_to_Ship_To');
            if (Trigger.newMap == null) {
                approvalRequest.setComments('Ship To created: ' + shipTo.Name);
            } else {
                approvalRequest.setComments('Ship To updated: ' + shipTo.Name);
            }

            Approval.ProcessResult result = Approval.process(approvalRequest);
            if (!result.isSuccess()) {
                Database.Error[] errors = result.getErrors();
                for (Database.Error error : errors) {
                    System.debug(error.getMessage());
                }
            }
        }
    }
}