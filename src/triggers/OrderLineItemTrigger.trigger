/*
 * 	Created by Jeffrey Zhou on 3/11/16
 * 	The SMB Help Desk, inc. 
 */

trigger OrderLineItemTrigger on Order_Line_Items__c (after update, after insert) {

    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        //OrderLineItemTriggerHelper.createLineItemConfigurations(Trigger.oldMap, Trigger.newMap);
    }
    
}