trigger AccountTrigger on Account (before insert, before update, after insert, after update) {

    Profile profile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
    RecordType VART = [Select id from RecordType where developerName = 'VA' and sobjectType = 'Account'];
    RecordType SubcontractorRT = [Select id from RecordType where developerName = 'Subcontractor' and sobjectType = 'Account'];
    RecordType DealerRT = [Select id from RecordType where developerName = 'Dealer' and sobjectType = 'Account'];
    // Only run diff if the user is not a system administrator
    if(profile.Name != 'System Administrator' && Trigger.isBefore) {
        List<SObject> sobjs = ObjectDiffer.diffAll(Trigger.oldMap, Trigger.newMap, Trigger.new);
        if (Trigger.isUpdate) {
            for (SObject sObj : sobjs) {
                if (sObj.get('RecordTypeId') == VART.id || sObj.get('RecordTypeId') == SubcontractorRT.id || sObj.get('RecordTypeId') == DealerRT.id) {
                    if (sObj.get('Inactive__c') == false) { // On every update, if not explicitly disabled, disable it
                        sObj.put('Inactive__c', true);
                        sObj.put('Inactive_Date__c', Date.today());
                        sObj.put('Inactive_Reason__c', 'User edited. Sent for approval.');
                    }
                    sObj.put('IsApproved__c', false);
                    sObj.put('Is_Rejected__c', false);
                    if (sObj.get('IsRecalled__c') == true) { // When recalled, revert object and clear flag.
                        ObjectDiffer.revert(sObj);
                        sObj.put('IsRecalled__c', false);
                        sObj.put('Diff__c', null);
                        if (sObj.get('Inactive__c') == true) { // Usually on update, it goes to inactive, activate when revert
                            sObj.put('Inactive__c', false);
                        }
                    }
                }
            }
        }
    }
    if(profile.Name == 'System Administrator' && Trigger.isBefore) {
        for (Account account : Trigger.new) {
            if (account.RecordTypeId == VART.id || account.RecordTypeId == SubcontractorRT.id || account.RecordTypeId == DealerRT.id) {
                if (account.Is_Rejected__c == true) {
                    if (account.DiffIsCreate__c != true) {
                        Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped(account.Diff__c);
                        if (!tempMap.containsKey('Inactive__c') && !(account.Name == 'DO NOT USE' || (tempMap.containsKey('Name') && tempMap.get('Name') == 'DO NOT USE'))) {
                            account.Inactive__c = !account.Inactive__c;
                        }
                        ObjectDiffer.revert(account);
                        account.Diff__c = null;
                    }
                }
                if (account.IsApproved__c == true) {
                    if (account.DiffIsCreate__c != true) {
                        if (account.Diff__c != null) {
                            Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped(account.Diff__c);
                            if (tempMap.containsKey('Inactive__c') && tempMap.get('Inactive__c') != true) {
                                account.Inactive__c = true;
                                account.Name = 'DO NOT USE';
                            } else {
                                account.Inactive__c = false;
                            }
                        } else {
                            account.Inactive__c = false;
                        }
                    } else {
                        account.Inactive__c = false;
                    }
                    account.Diff__c = null;
                }
            }
        }
    }
    if (profile.Name != 'System Administrator' && Trigger.isAfter) {
        for (Account account : Trigger.new) {
            if (account.RecordTypeId == VART.id || account.RecordTypeId == SubcontractorRT.id || account.RecordTypeId == DealerRT.id) {
                if ((account.Diff__c == null && account.DiffIsCreate__c != true) || account.Diff__c == '{}') { return; }
                List<ProcessInstance> instances = [Select id from ProcessInstance where targetObjectId = :account.id and status = 'Pending'];
                if (instances.size() > 0) {
                    return;
                } // if item is already in approval due to workflow updates.
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                User user = [Select id, ManagerId from User where id = :UserInfo.getUserId()];
                approvalRequest.setSubmitterId(UserInfo.getUserId());
                approvalRequest.setNextApproverIds(new List<Id>{
                        user.ManagerId
                });
                approvalRequest.setObjectId(account.Id);
                approvalRequest.setProcessDefinitionNameOrId('Changes_to_Account');
                if (Trigger.newMap == null) {
                    approvalRequest.setComments('Account created: ' + contact.Name);
                } else {
                    approvalRequest.setComments('Account updated: ' + contact.Name);
                }

                Approval.ProcessResult result = Approval.process(approvalRequest);
                if (!result.isSuccess()) {
                    Database.Error[] errors = result.getErrors();
                    for (Database.Error error : errors) {
                        System.debug(error.getMessage());
                    }
                }
            }
        }
    }
}