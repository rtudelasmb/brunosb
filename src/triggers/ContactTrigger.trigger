trigger ContactTrigger on Contact (before insert, before update, after insert, after update) {

    Profile profile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
    // Only run diff if the user is not a system administrator
    if(profile.Name != 'System Administrator' && Trigger.isBefore) {
        List<SObject> sobjs = ObjectDiffer.diffAll(Trigger.oldMap, Trigger.newMap, Trigger.new);
        if (Trigger.isUpdate) {
            for (SObject sObj : sobjs) {
                if (sObj.get('Inactive__c') == false) { // On every update, if not explicitly disabled, disable it
                    sObj.put('Inactive__c', true);
                }
                sObj.put('IsApproved__c', false);
                sObj.put('Is_Rejected__c', false);
                if (sObj.get('IsRecalled__c') == true) { // When recalled, revert object and clear flag.
                    ObjectDiffer.revert(sObj);
                    sObj.put('IsRecalled__c', false);
                    sObj.put('Diff__c', null);
                    if (sObj.get('Inactive__c') == true) { // Usually on update, it goes to inactive, activate when revert
                        sObj.put('Inactive__c', false);
                    }
                }
            }
        }
    }
    if(profile.Name == 'System Administrator' && Trigger.isBefore) {
        for (Contact contact : Trigger.new) {
            if (contact.Is_Rejected__c == true) {
                if (contact.DiffIsCreate__c != true) {
                    Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped(contact.Diff__c);
                    if (!tempMap.containsKey('Inactive__c') && !(contact.LastName == 'DO NOT USE' || (tempMap.containsKey('LastName') && tempMap.get('LastName') == 'DO NOT USE'))) {
                        contact.Inactive__c = !contact.Inactive__c;
                    }
                    ObjectDiffer.revert(contact);
                    contact.Diff__c = null;
                }
            }
            if (contact.IsApproved__c == true) {
                if (contact.DiffIsCreate__c != true) {
                    if (contact.Diff__c != null) {
                        Map<String, Object> tempMap = (Map<String, Object>) JSON.deserializeUntyped(contact.Diff__c);
                        if (tempMap.containsKey('Inactive__c') && tempMap.get('Inactive__c') != true) {
                            contact.Inactive__c = true;
                            contact.LastName = 'DO NOT USE';
                            contact.FirstName = null;
                        } else {
                            contact.Inactive__c = false;
                        }
                    } else {
                        contact.Inactive__c = false;
                    }
                } else {
                    contact.Inactive__c = false;
                }
                contact.Diff__c = null;
            }
        }
    }
    if (profile.Name != 'System Administrator' && Trigger.isAfter) {
        for (Contact contact : Trigger.new) {
            List<ProcessInstance> instances = [Select id from ProcessInstance where targetObjectId = :contact.id and status = 'Pending'];
            if (instances.size() > 0) {return;} // if item is already in approval due to workflow updates.
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            User user = [Select id, ManagerId from User where id = :UserInfo.getUserId()];
            approvalRequest.setSubmitterId(UserInfo.getUserId());
            approvalRequest.setNextApproverIds(new List<Id>{user.ManagerId});
            approvalRequest.setObjectId(contact.Id);
            approvalRequest.setProcessDefinitionNameOrId('Changes_to_Contact');
            if (Trigger.newMap == null) {
                approvalRequest.setComments('Contact created: ' + contact.Name);
            } else {
                approvalRequest.setComments('Contact updated: ' + contact.Name);
            }

            Approval.ProcessResult result = Approval.process(approvalRequest);
            if (!result.isSuccess()) {
                Database.Error[] errors = result.getErrors();
                for (Database.Error error : errors) {
                    System.debug(error.getMessage());
                }
            }
        }
    }
}