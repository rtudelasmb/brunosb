/**
 * Created by Ryne on 2/19/2016.
 */

trigger RebateTrigger on Rebate__c (before insert, before update) {
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        RebateTriggerHelper.gatherNumbers(trigger.new);
    }
}